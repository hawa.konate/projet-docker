# Use the official Python 3 base image from Docker Hub
FROM python:3

# Create a directory for the application in the container
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Set environment variables to optimize Python behavior in containers
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Upgrade pip to the latest version
RUN pip install --upgrade pip

# Copy the requirements file and the main Django manage.py script into the container
COPY ./requirements.txt .
COPY ./manage.py .

# Install the Python dependencies specified in requirements.txt
RUN pip install -r requirements.txt

# Copy initialization scripts for MongoDB and PostgreSQL into the appropriate directories
COPY dbinits/init-mongo.js /docker-entrypoint-initdb.d/init-mongo.js
COPY dbinits/init-postgres.sql /docker-entrypoint-initdb.d/init-postgres.sql

# Adjust permissions on the initialization scripts
RUN chmod +rx /docker-entrypoint-initdb.d/init-postgres.sql
RUN chmod +rx /docker-entrypoint-initdb.d/init-mongo.js

# Copy the entire application code into the container
COPY . .

# Expose port 8000 to the outside world
EXPOSE 8000

# Specify the default command to run when the container starts
CMD python manage.py runserver
