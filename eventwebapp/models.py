from django.db import models
from bson import ObjectId


class User(models.Model):
    id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    email = models.EmailField()
    birthdate = models.DateField()
    event = models.CharField(max_length=255)

    class Meta:
        db_table = 'users'

    def __str__(self):
        return f"{self.firstname} {self.lastname}"


class Event(models.Model):
    _id = ObjectId(),
    name = models.CharField(max_length=200),
    date = models.DateField(),
    attendees = models.ManyToManyField(User, related_name="event_attendees")

    class Meta:
        db_table = 'events'

    def __str__(self):
        return f"{self.name} on {self.date}"
