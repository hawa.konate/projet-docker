from django import forms
from django.conf import settings
from pymongo import MongoClient


class SearchForm(forms.Form):
    firstname = forms.CharField(label="First Name", max_length=100, required=True)
    lastname = forms.CharField(label="Last Name", max_length=100, required=True)
    email = forms.EmailField(label="Email", max_length=100, required=True)


class RegistrationForm(forms.Form):
    firstname = forms.CharField(label="First Name", max_length=100, required=True)
    lastname = forms.CharField(label="Last Name", max_length=100, required=True)
    email = forms.EmailField(label="Email", max_length=100, required=True)
    birthdate = forms.DateField(label="Date of Birth", required=True, widget=forms.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, **kwargs):
        super(RegistrationForm, self).__init__(*args, **kwargs)

        mongo_client = MongoClient(settings.MONGO_DB_URI)
        mongo_db = mongo_client['events']
        events_collection = mongo_db['events']
        events_data = list(events_collection.find())

        self.fields['event'] = forms.ChoiceField(
            label="Event",
            choices=[(str(event['_id']), f"{event['name']} on {event['date']}") for event in events_data]
        )
