from django.shortcuts import render
from django.conf import settings
from pymongo import MongoClient
from .models import User
from .forms import SearchForm, RegistrationForm
from bson import ObjectId

mongoClient = MongoClient(settings.MONGO_DB_URI)
mongoDb = mongoClient['events']
events_collection = mongoDb['events']

def home(request):
    message = None

    events_data = events_collection.find()
    registration_form = RegistrationForm()

    event_choices = [(str(event['_id']), f"{event['name']} on {event['date']}") for event in events_data]
    registration_form.fields['event'].choices = event_choices

    if request.method == 'POST':
        registration_form = RegistrationForm(request.POST)
        if registration_form.is_valid():
            firstname = registration_form.cleaned_data['firstname']
            lastname = registration_form.cleaned_data['lastname']
            email = registration_form.cleaned_data['email']
            birthdate = registration_form.cleaned_data['birthdate']
            event_id = registration_form.cleaned_data['event']

            existing_user = User.objects.filter(firstname=firstname, lastname=lastname, email=email,
                                                event=event_id)

            if not existing_user:
                user = User.objects.create(
                    firstname=firstname,
                    lastname=lastname,
                    email=email,
                    birthdate=birthdate,
                    event=event_id
                )

                event = events_collection.find_one({'_id': ObjectId(event_id)})

                if event:

                    event['attendees'].append(user.email)
                    events_collection.update_one({'_id': ObjectId(event_id)}, {'$set': {'attendees': event['attendees']}})
                    user.save()

                    message = "Well registered !"
                else:
                    message = "An error occurred while registering at the event"
            else:
                message = "You are already attending this event"

        return render(request, 'index.html', {'registration_form': RegistrationForm, 'message': message})
    return render(request, 'index.html', {'registration_form': RegistrationForm})


def dashboard(request):
    message = ""
    attended_events_info = []

    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            firstname = form.cleaned_data['firstname']
            lastname = form.cleaned_data['lastname']
            email = form.cleaned_data['email']

            person = User.objects.filter(firstname=firstname, lastname=lastname, email=email)

            if person.exists():
                events = events_collection.find({})
                attended_events = []

                for event in events:
                    if email in event.get('attendees', []):
                        attended_events.append(event)
                        attended_events_info.append({'name': event['name'], 'date': event['date']})

                if not attended_events:
                    message = "You are not attending any events."

            else:
                message = "User not found in the database."
    return render(request, 'dashboard.html', {'form': SearchForm(), 'message': message, 'attended_events_info': attended_events_info})
