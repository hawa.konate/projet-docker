-- Set the owner of the 'event_registration' database to 'event_registration_app'
ALTER DATABASE event_registration OWNER TO event_registration_app;
-- Connect to the 'event_registration' database
\connect event_registration;

-- Create the 'users' table with necessary columns
CREATE TABLE users (
    "id" serial PRIMARY KEY,
    "firstname" varchar(255),
    "lastname" varchar(255),
    "email" varchar(255),
    "birthdate" date,
    "event" varchar(255)
);

ALTER TABLE users OWNER TO event_registration_app;
