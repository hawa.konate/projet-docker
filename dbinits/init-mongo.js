// MongoDB script to create a user for the 'events' database
db.createUser({
  user: 'event_registration_app',
  pwd: 'eventadmin',
  roles: [
    {
      role: 'readWrite',
      db: 'events',
    }
  ]
});

// Insert initial events into the 'events' collection
db.events.insertMany([
    { name: 'Startup Networking Event', date: ISODate("2024-01-01T12:00:00Z"), attendees: [] },
    { name: 'Mobile App Launch Party', date: ISODate("2024-02-01T14:00:00Z"), attendees: [] },
    { name: 'Data Science Conference', date: ISODate("2024-02-01T14:00:00Z"), attendees: [] },
    { name: 'Python Engineering Conference', date: ISODate("2024-04-01T14:00:00Z"), attendees: [] },
    { name: 'AI Summit', date: ISODate("2024-05-01T10:00:00Z"), attendees: [] },
    { name: 'Blockchain Expo', date: ISODate("2024-06-01T13:30:00Z"), attendees: [] },
    { name: 'Web Development Workshop', date: ISODate("2024-07-01T15:00:00Z"), attendees: [] },
    { name: 'Cloud Computing Conference', date: ISODate("2024-08-01T11:00:00Z"), attendees: [] },
    { name: 'Tech Innovation Forum', date: ISODate("2024-09-01T09:30:00Z"), attendees: [] },
    { name: 'Cybersecurity Summit', date: ISODate("2024-10-01T12:30:00Z"), attendees: [] },
    { name: 'UX/UI Design Workshop', date: ISODate("2024-11-01T14:30:00Z"), attendees: [] },
    { name: 'Big Data Symposium', date: ISODate("2024-12-01T16:00:00Z"), attendees: [] },
    { name: 'Robotics Expo', date: ISODate("2025-01-01T09:00:00Z"), attendees: [] },
    { name: 'Virtual Reality Conference', date: ISODate("2025-02-01T13:00:00Z"), attendees: [] },
    { name: 'Internet of Things (IoT) Summit', date: ISODate("2025-03-01T10:00:00Z"), attendees: [] },
    { name: 'Machine Learning Showcase', date: ISODate("2025-04-01T13:30:00Z"), attendees: [] },
    { name: 'Frontend Frameworks Workshop', date: ISODate("2025-05-01T15:00:00Z"), attendees: [] },
    { name: 'DevOps Conference', date: ISODate("2025-06-01T11:00:00Z"), attendees: [] },
    { name: 'E-commerce Innovation Expo', date: ISODate("2025-07-01T09:30:00Z"), attendees: [] },
    { name: 'Augmented Reality Symposium', date: ISODate("2025-08-01T12:00:00Z"), attendees: [] },
    { name: 'Quantum Computing Forum', date: ISODate("2025-09-01T14:00:00Z"), attendees: [] },
    { name: 'Mobile Gaming Festival', date: ISODate("2025-10-01T16:30:00Z"), attendees: [] },
    { name: 'HealthTech Summit', date: ISODate("2025-11-01T09:30:00Z"), attendees: [] },
    { name: 'Startup Pitch Competition', date: ISODate("2025-12-01T13:00:00Z"), attendees: [] },
    { name: 'Artificial Intelligence Expo', date: ISODate("2026-01-01T10:00:00Z"), attendees: [] },
    { name: 'Cybersecurity Hackathon', date: ISODate("2026-02-01T13:30:00Z"), attendees: [] },
    { name: 'Blockchain Development Workshop', date: ISODate("2026-03-01T15:00:00Z"), attendees: [] },
    { name: 'AI in Healthcare Symposium', date: ISODate("2026-01-01T10:00:00Z"), attendees: [] },
    { name: 'Fintech Innovation Summit', date: ISODate("2026-02-01T13:30:00Z"), attendees: [] },
    { name: 'Agile Development Workshop', date: ISODate("2026-03-01T15:00:00Z"), attendees: [] },
    { name: 'Smart Cities Expo', date: ISODate("2026-04-01T11:00:00Z"), attendees: [] },
    { name: 'Diversity in Tech Conference', date: ISODate("2026-05-01T09:30:00Z"), attendees: [] },
    { name: 'Game Developers Forum', date: ISODate("2026-06-01T12:00:00Z"), attendees: [] },
    { name: 'IoT Security Summit', date: ISODate("2026-07-01T14:00:00Z"), attendees: [] },
    { name: 'AR/VR Gaming Expo', date: ISODate("2026-08-01T16:30:00Z"), attendees: [] },
    { name: 'BioTech Innovators Conference', date: ISODate("2026-09-01T09:30:00Z"), attendees: [] }
   
   
])

